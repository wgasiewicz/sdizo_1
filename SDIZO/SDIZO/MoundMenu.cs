﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SDIZO
{
    public partial class MoundMenu : Form
    {
        Mound mound = new Mound();
        Functions call = new Functions();
        public MoundMenu()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {//dodaj element na pierwsze miejsce            ;
            if (call.isNumeric(textBox2.Text) == true )
            {
                Stopwatch stopwatch = Stopwatch.StartNew();
                mound.Push(Convert.ToInt32(textBox2.Text));
                stopwatch.Stop();
                richTextBox1.Text = mound.ToString();
                textBox1.Text = stopwatch.Elapsed.ToString();
            }
            else
            {
                MessageBox.Show("Podaj poprawną liczbę!", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBox2.Clear();
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {//wybor pliku
            mound.ReadFromFile(call.chooseFile());//wybor pliku z eksploratora windows
            richTextBox1.Text = mound.ToString();//wyswietlenie
        }

        private void button8_Click(object sender, EventArgs e)
        {//wyswietl kopiec
            richTextBox1.Text=mound.ToString();
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {//textbox

        }

        private void button1_Click(object sender, EventArgs e)
        {//usun pierwszy            
            Stopwatch stopwatch = Stopwatch.StartNew();
            mound.DeleteFirst();
            stopwatch.Stop();
            richTextBox1.Text = mound.ToString();
            textBox1.Text = stopwatch.Elapsed.ToString();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {//pomiar czasu

        }

        private void button5_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {//czyszczenie textboxa
            richTextBox1.Clear();
           // mound.Clear();//czyszczenie kopca
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {//element do dodania

        }

        private void button4_Click(object sender, EventArgs e)
        {//wyszukaj element o zadanej wartosci
            if (call.isNumeric(textBox3.Text) == true)
            {
                Stopwatch stopwatch = Stopwatch.StartNew();
                mound.Search(Convert.ToInt32(textBox3.Text));
                stopwatch.Stop();
                textBox1.Text = stopwatch.Elapsed.ToString();
                richTextBox1.Text = mound.ToString();
                if (mound.Search(Convert.ToInt32(textBox3.Text)) == true)
                {
                    MessageBox.Show("Podany element istnieje w kopcu!", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Podany element nieistnieje w kopcu!", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                textBox3.Clear();
            }
            else
            {
                MessageBox.Show("Podaj poprawną liczbę!", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBox3.Clear();
            }
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {//element do wyszukania

        }
    }
}
