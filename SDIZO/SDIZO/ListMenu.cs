﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SDIZO
{
    public partial class ListMenu : Form
    {
        DoubleList list = new DoubleList();
        Functions call = new Functions();
        public ListMenu()
        {
            InitializeComponent();
        }

        private void button9_Click(object sender, EventArgs e)
        {//wybierz plik
            list.ReadFromFile(list,call.chooseFile());//wybranie pliku z danymi
            richTextBox1.Text = list.ToString();//wyswietlenie
        }

        private void button2_Click(object sender, EventArgs e)
        {//dodaj na pierwsze miejsce
            if (call.isNumeric(textBox2.Text) == true)
            {
                Stopwatch stopwatch = Stopwatch.StartNew();
                list.PushFirst(list,Convert.ToInt32(textBox2.Text));
                stopwatch.Stop();
                richTextBox1.Text = list.ToString();
                textBox1.Text=stopwatch.Elapsed.ToString();
            }
            else
            {
                MessageBox.Show("Podaj poprawną liczbę!", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBox2.Clear();
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {//dodaj w dowolne miejsce
            if (call.isNumeric(textBox4.Text) && list.TestIndex(Convert.ToInt32(textBox4.Text)) == true && call.isNumeric(textBox2.Text))
            {
                Stopwatch stopwatch = Stopwatch.StartNew();
                list.PushAtAny(Convert.ToInt32(textBox2.Text),Convert.ToInt32(textBox4.Text),list);
                stopwatch.Stop();
                richTextBox1.Text = list.ToString();
                textBox1.Text = stopwatch.Elapsed.ToString();
            }
            else
            {
                MessageBox.Show("Podaj poprawną liczbę!", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBox4.Clear(); textBox2.Clear();
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {//dodaj na koniec
            if (call.isNumeric(textBox2.Text) == true)
            {
                Stopwatch stopwatch = Stopwatch.StartNew();
                list.PushAtLast(list,Convert.ToInt32(textBox2.Text));
                stopwatch.Stop();
                richTextBox1.Text = list.ToString();
                textBox1.Text = stopwatch.Elapsed.ToString();
            }
            else
            {
                MessageBox.Show("Podaj poprawną liczbę!", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBox2.Clear();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {//usun pierwszy element
                Stopwatch stopwatch = Stopwatch.StartNew();
                list.DeleteFirst();
                stopwatch.Stop();
                richTextBox1.Text = list.ToString();
                textBox1.Text = stopwatch.Elapsed.ToString();            
        }

        private void button6_Click(object sender, EventArgs e)
        {//usun element o zadanym indeksie
            if (call.isNumeric(textBox5.Text)&& list.TestIndex(Convert.ToInt32(textBox5.Text))==true) {
                Stopwatch stopwatch = Stopwatch.StartNew();
                list.DeleteAny(Convert.ToInt32(textBox5.Text));
                stopwatch.Stop();
                richTextBox1.Text = list.ToString();
                textBox1.Text = stopwatch.Elapsed.ToString();
            }
            else
            {
                MessageBox.Show("Podaj poprawną liczbę!", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBox5.Clear();
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {//usun ostatni element
            Stopwatch stopwatch = Stopwatch.StartNew();
            list.DeleteLast(list);
            stopwatch.Stop();
            richTextBox1.Text = list.ToString();
            textBox1.Text = stopwatch.Elapsed.ToString();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {//element do dodania

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {//element do usuniecia

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {//ilosc elementow???

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {// miejsce do dodania

        }

        private void button8_Click(object sender, EventArgs e)
        {//wyswietl liste
            richTextBox1.Text=list.ToString();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {//czas operacji

        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {//czszczenie
            richTextBox1.Clear();
        }

        private void button10_Click(object sender, EventArgs e)
        {//wyzukaj element o danej wartosci
            if (call.isNumeric(textBox3.Text) == true)
            {
                Stopwatch stopwatch = Stopwatch.StartNew();
                list.Search(Convert.ToInt32(textBox3.Text));
                stopwatch.Stop();
                textBox1.Text = stopwatch.Elapsed.ToString();
                richTextBox1.Text = list.ToString();
                if (list.Search(Convert.ToInt32(textBox3.Text)) == true)
                {
                    MessageBox.Show("Podany element istnieje w liście!", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Podany element nieistnieje w liście!", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                textBox3.Clear();
            }
            else
            {
                MessageBox.Show("Podaj poprawną liczbę!", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBox3.Clear();
            }
        }

        private void textBox3_TextChanged_1(object sender, EventArgs e)
        {//element do wyszukania

        }
    }
}
