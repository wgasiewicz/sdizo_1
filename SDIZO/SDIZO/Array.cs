﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
namespace SDIZO
{
    class Array
    {
        private int size, head;//rozmiar,pierwszy element
        int[] tab;//tablica
        int[] temp1 = new int[0];//pomocnicza tablica
        public Array(int size, int head)
        {
            tab = new int[size];
            this.size = size;
            this.head = tab[0];
        }
        public Array()
        {
            this.size = 0;
            this.head = 0;
        }

        //dodawanie na poczatek tablicy
        public void push(int value)
        {//dodanie do tablicy
            createTab(size, value);
            size++;//zwiekszenie rozmiaru
        }
        public override string ToString()
        {
            if (tab == null) return "";//tablica pusta
            else
            {
                string temp = "";//pomocniczy string
                for (int i = 0; i < tab.Length; i++)
                {
                    temp += tab[i].ToString() + Environment.NewLine;//zbieranie kolejnych wartosci tablicy do stringa
                }
                return temp;//zwracamy stringa
            }
        }
        private void createTab(int newSize, int value)//dodawanie na poczatek
        {
            cloneTab();//kopiowanie tablicy
            tab = new int[newSize + 1];//tworzymy nowa tablice o zwiekszonym o 1 rozmiarze
            tab[0] = value;//za pierwszy element podstawiamy nowa wartosc
            int currentIndex = 1;//indeks pomocniczy od 1
            foreach (int data in temp1)//dla kazdego elementu w pomocniczej tablicy
            {
                tab[currentIndex] = data;//przepisujemy wartosc
                currentIndex++;//zwiekszamy indeks
            }
        }
        private void cloneTab()//kopiowanie aktualnej tablicy
        {
            int currentIndex = 0;//ustawiamy pomocniczy indeks na 0
            temp1 = new int[tab.Length];//tworzymy pomocnicza tablice
            foreach (int data in tab)//dla kzdej wartosci z dotychczasowej talicy
            {
                temp1[currentIndex] = data;//kopiujemy do tablicy pomocniczej
                currentIndex++;//zwiekszamy indeks
            }
            currentIndex = 0;//ustawiamy ineks na 0
        }
        private void createTabPushBack(int newSize, int value)//dodawanie na koniec
        {
            cloneTab();//koiujemy tablice
            tab = new int[newSize + 1];//tworzymy nowa tablice o zwiekszonym rozmiarze
            int currentIndex = 0;//ustawiamy index na 0
            foreach (int data in temp1)//dla kazdejwartosci w pomocniczej tablicy
            {
                tab[currentIndex] = data;//kopiujemy do nowej tavlicy
                currentIndex++;//zwiekszamy index
            }
            tab[newSize] = value;//na ostatnie mijsce dodajemy nowy element
        }
        //dodawanie na koniec
        public void pushBack(int value)
        {
            createTabPushBack(size, value);//wywolujemy funkcje dodawania na koniec
            size++;//zwiekszamy rozmiar
        }
        private void CreateTabAny(int newSize, int value, int where)
        {
            cloneTab();//kopiujemy tablice
            tab = new int[newSize];//tworzymy nowa talice
            bool found = false;//zmienna pomocnicza do znalezienia miejsca dodania
            for (int i = 0; i < newSize; i++)//dla kazdego elementu w tablicy
            {
                if (i != where)//jezeli aktualny index != od zadanego
                {
                    if (found)//jezeli znaleziono juz zadane miejsce
                        tab[i] = temp1[i - 1];//do nowej tablicy wpisujemy wartosci ze starej cofniete o 1 
                    else tab[i] = temp1[i];//jezeli nie znalziono kopiujemy elementy rownolegle
                }
                if (i == where)//jezeli index == zadanj pozycji
                {
                    found = true;//ustawiamy found na true
                    tab[i] = value;//na zadana pozycje dodajemy wartosc
                }
            }
        }
        //dodawanie na dowolne miejsce
        public void pushAny(int value, int where)
        {
                size++;//zwiekszamy rozmiar
                CreateTabAny(size, value, where);//wywolujemy funkcje do wstawiania w zadane miejsce
        }
        //usuwanie elementu o zadanym indexie
        public void deleteIndex(int index)
        {
            int j = index;//zmienna pomocnicza
            if (size == 0)   //jezeli tablica pusta konczymy         
                return;
            else if (index < 0 || index > size) return;//jezeli index jest nieprawidlowy konczymy
            else
            {
                cloneTab();//kopiujemy tablice
                tab = new int[size-1];//tworzymy nowa tablice o rozmiarze o 1 mniejszym
                for (int i = 0; i < index; i++)//dla kzdego elementu az do wybranego indexu
                {
                    tab[i] = temp1[i];//kopiujemy tablice
                }
                for (int i = index; i < size; i++)//dla kazdego elementu od indexu do konca tablicy
                {
                    if (i + 1 == size) break;//jezeli kolejny index to rozmiar konczymy
                    else
                    {
                        tab[j] = temp1[i + 1];//kopiujemy elementu do tablicy z przeskokiem o 1
                        j++;//ziwekszamy zmienna pomocnicza
                    }
                }
                size--;//zmniejszamy rozmiar
            }
        }
        //usuwanie pierwszego elementu
        public void deleteFirst()
        {
            if (size == 0)//jezeli tablica pusta konczymy
                return;
            else
                deleteIndex(0);//wywolujemy funkcje usuwajaca dowolny element z parametrem 0
        }
        //usuwanie ostatniego elementu
        public void deleteLast()
        {
            if (size == 0)//jezeli tablica pusta konczymy
                return;

            else
                deleteIndex(size-1);//wywolujemy funkcje usuwajaca dowolny element z indexem ostatniego elelemntu
        }
        public void readFromFile(string path)
        {//odczyt z pliku
            StreamReader sr = new StreamReader(path);
            string line;//zmianna pomocnicza do odczytywania kolejnych linii z pliku
            int i = 0;//licznik linii
            while ((line = sr.ReadLine()) != null)//dopoki w danej linii jest jakas wartosc
            {
                if (i == 0) { size = Convert.ToInt32(line); tab = new int[size]; }//pierwsza wartosc to rozmiar, tworzymy tablice o tym rozmirze
                else
                {
                    tab[i - 1] = Convert.ToInt32(line);//wczytujemy do tablicy kolejne wartosci
                }
                i++;//zwiekszamy licznik
            }
            i = 0;//zerujemy licznik
        }
        public Boolean Search(int value)
        {//wyszukiwanie podanego elementu
            for(int i=0;i<size;i++)//dla kazdego elementu w tablicy 
            {
                if (tab[i] == value)//sprawdzamy czy == szukanej wartosci
                {
                    return true;//jak tak zracamy true
                }
            }
            return false;//jesli nie zwracamy false
        }
        
        public Boolean testIndex(int index)//sprawdzenie czy zadany index nie przekracza rozmiaru
        {
            if (index < 0 || index > size)//jezeli index <0 lub > niz rozmir
                return false;//zwracamy false
            else return true;//inaczej zwracamy true
        }
    }
}
