﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
namespace SDIZO
{
    public partial class Menu : Form
    {
       public string path;
        public Menu()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {//tablica
            ArrayMenu am = new ArrayMenu();
            am.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {//lista
            ListMenu lm = new ListMenu();
            lm.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {//kopiec
            MoundMenu mm = new MoundMenu();
            mm.ShowDialog();
        }

        private void Menu_Load(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {//generuj plik do testow
           // if (textBox1.Text != "" && Convert.ToInt32(textBox1.Text).Equals(true))
           try
            {
                Random rand = new Random();
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.Filter = "Text File|*.txt";
                sfd.ShowDialog();
                path = sfd.FileName;
                StreamWriter sw = new StreamWriter(File.Create(path));
                sw.Write(textBox1.Text + Environment.NewLine);
                try
                {
                    for (int i = 0; i < Convert.ToInt32(textBox1.Text); i++)
                    {
                        string add = rand.Next(1000, 9999).ToString() + Environment.NewLine;//losuje liczby od 1000 do 9999 i dodaje do stringa
                        sw.Write(add);//wpisanie stringa do pliku
                    }

                    sw.Dispose();
                    MessageBox.Show("Wygenerowano pomyślnie!", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    textBox1.Clear();
                }
                catch (Exception ex) { MessageBox.Show(ex.ToString(), "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            }
            catch(Exception ex) { MessageBox.Show("Podaj poprawną ilość liczb do wygenerowania!", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {//ile liczb wygenerowac

        }
        public string getPath()
        { return path; }

        private void button5_Click(object sender, EventArgs e)
        {//drzewo bst
            BSTMenu bst = new BSTMenu();
            bst.ShowDialog();
        }
    }
}
