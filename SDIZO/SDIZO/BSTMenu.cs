﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SDIZO
{
    public partial class BSTMenu : Form
    {
        BSTTree tree = new BSTTree();
        Functions call = new Functions();
        public BSTMenu()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {//wstaw wezel o kluczu k
            if(call.isNumeric(textBox2.Text) == true)
            {            
                Stopwatch stopwatch = Stopwatch.StartNew();
                tree.PushKey(Convert.ToInt32(textBox2.Text));
                stopwatch.Stop();
                richTextBox1.Text = tree.ToString();
                textBox1.Text = stopwatch.Elapsed.ToString();
                textBox2.Clear();
            }
            else
            {
                MessageBox.Show("Podaj poprawną liczbę!", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                 textBox2.Clear();
            }

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {//klucz do dodania

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {//czas

        }

        private void button8_Click(object sender, EventArgs e)
        {//wyswietl drzewo
            richTextBox1.Text = tree.ToString();
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {//element do wyszukania

        }

        private void button4_Click(object sender, EventArgs e)
        {//wyszukaj element o danej wartosci
            if (call.isNumeric(textBox3.Text) == true)
            {
                Stopwatch stopwatch = Stopwatch.StartNew();
                tree.Search(Convert.ToInt32(textBox3.Text));
                stopwatch.Stop();
                textBox1.Text = stopwatch.Elapsed.ToString();
                richTextBox1.Text = tree.ToString();
                if (tree.Search(Convert.ToInt32(textBox3.Text)) == true)
                {
                    MessageBox.Show("Podany element istnieje w drzewie!", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Podany element nieistnieje w drzewie!", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                textBox3.Clear();
            }
            else
            {
                MessageBox.Show("Podaj poprawną liczbę!", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBox3.Clear();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {//wyczysc
            richTextBox1.Clear();
        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {//element do usuniecia

        }

        private void button6_Click(object sender, EventArgs e)
        {//usun elelent o zadanym indeksie

        }

        private void button9_Click(object sender, EventArgs e)
        {//wybirz plik z danymi
            tree.ReadFromFile(call.chooseFile());
            richTextBox1.Text = tree.ToString();
        }
    }
}
