﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SDIZO
{
    class BSTNode
    {
        public BSTNode up, left, right;
        public int key;

        public BSTNode(int k)
        {
            this.key = k;
            up = null;
            left = null;
            right = null;
        }
        public BSTNode()
        {
            up = null;
            left = null;
            right = null;
        }
    }
}
