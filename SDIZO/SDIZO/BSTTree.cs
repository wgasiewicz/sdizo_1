﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SDIZO
{
    class BSTTree
    {
        BSTNode head;//korzen
        int size = 0;
        string temp = "";
        public void PushKey(int key)
        {//dodawanie wezla do drzewa
            BSTNode newNode = new BSTNode(key);
            
            if (head == null)//jezeli drzewo puste
            {
                head = newNode;//nowy element jest wierzcholkiem
            }
            else
            {
                BSTNode temp = head;//wpisujemy head do pomocniczego wierzcholka
                BSTNode parent;//rodzic
                while (true)
                {
                    parent = temp;//rodzic przyjmuje wartosc pomocniczego wierzcholka
                    if (key < temp.key)//jezeli podana wartosc < od aktualnej
                    {
                        temp = temp.left;//wpisjemy lewego syna head do pomocniczego wierzcholka
                        if (temp == null)//jezeli temp nie istnieje
                        {
                            parent.left = newNode;//do lewego syna wpisujemy nowy wierzcholek
                            size++;//zwiekszamy rozmiar
                            return;//koniec
                        }
                    }
                    else
                    {
                        temp = temp.right;//wpisujemy prawego syna head do pomocniczego wierzcholka
                        if(temp==null)//jezeli temp nie istnieje
                        {
                            parent.right = newNode;
                            size++;
                            return;
                        }
                    }
                }
            }
            size++;
        }
        private BSTNode GetLeft(BSTNode left)
        {//funkcja zwraca lewego syna zadanego wezla
            return left.left;
        }
        private BSTNode GetRight(BSTNode right)
        {//zwraca prawego syna
            return right.right;
        }       
        string VisitAllNodes(BSTNode actuall)
        {//przejscie in-order
            if(actuall!=null)
            {                
                VisitAllNodes(actuall.left);
                temp += actuall.key + "\n";
                VisitAllNodes(actuall.right);
            }
            return temp;
        }
        public override string ToString()
        {//metoda wyswietlajaca drzewo
            temp = "";
            BSTNode actual = head;
            if (size == 0) return temp;
            else
            {
                temp+=VisitAllNodes(actual);
                return temp;
            }
        }
        public Boolean Search(int value)
        {//wyszukiwanie danego elementu
            BSTNode temp = head;
            if (temp == null) return false;//jezeli drzewo puste, nie znaleziono


            while(temp!=null)
            {
                if (temp.key == value) return true;//znaleziono element
                if (temp.left.key < temp.key)
                    temp = temp.left;
                else
                    temp = temp.right;
            }
            return false;
        }
        public void ReadFromFile(string path)
        {//wczytywanie z pliku
            StreamReader sr = new StreamReader(path);
            string line;//string do odczytu linii
            int i = 0;//licznik linii
            while ((line = sr.ReadLine()) != null)//dopoki w aktualnej linii jest jakas wartosc
            {
                if (i == 0) { size = Convert.ToInt32(line); }//pierwsza linia to rozmiar
                else
                {
                    PushKeyForReadFromFile(Convert.ToInt32(line));//wpisujemy do drzewa kolejne wartosci
                }
                i++;
            }
            i = 0;
        }
        public void PushKeyForReadFromFile(int key)
        {//to samo co dodawanie wierzcholka, nie zwieksza rozmiaru - tylko do wczytania z pliku
            BSTNode newNode = new BSTNode(key);

            if (head == null)//jezeli drzewo puste
            {
                head = newNode;//nowy element jest wierzcholkiem
            }
            else
            {
                BSTNode temp = head;//wpisujemy head do pomocniczego wierzcholka
                BSTNode parent;//rodzic
                while (true)
                {
                    parent = temp;//rodzic przyjmuje wartosc pomocniczego wierzcholka
                    if (key < temp.key)//jezeli podana wartosc < od aktualnej
                    {
                        temp = temp.left;//wpisjemy lewego syna head do pomocniczego wierzcholka
                        if (temp == null)//jezeli temp nie istnieje
                        {
                            parent.left = newNode;//do lewego syna wpisujemy nowy wierzcholek
                            return;//koniec
                        }
                    }
                    else
                    {
                        temp = temp.right;//wpisujemy prawego syna head do pomocniczego wierzcholka
                        if (temp == null)//jezeli temp nie istnieje
                        {
                            parent.right = newNode;
                            return;
                        }
                    }
                }
            }
        }
    }
}
