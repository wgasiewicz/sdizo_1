﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SDIZO
{
    class Mound
    {
        int size;
        int[] mount;
        int[] tempMound;
        int w = 0;//ilość wezlow

        public void Push(int add)
        {//dodawanie do kopca         
            CloneMound();//kopiowanie dotychczasowego kopca
            CreatePush(size, add);//dodawanie nowego elementu na miejsce ostatnie
            size++;
            SortNew();//sortowanie kopca z nowym elementem
        }
        private void CreatePush(int newSize,int add)
        {
            mount = new int[newSize + 1];//tworzymy nowy kopiec, zwiekzamy rozmiar o 1
            int currentIndex = 0;//zaczynamy od 0

            if (newSize + 1 == 1) { mount[newSize] = add; }//jezeli kopiec byl pusty na pierwsze miejsce dodajemy zadana wartosc
            else
            {
                foreach (int data in tempMound)
                {
                    mount[currentIndex] = data;//wpisujemy do kopca wszystkie dane z tymczasowego kopca
                    currentIndex++;//zwiekszamy indeks
                }
                mount[newSize] = add;//dodajemy wartosc na ostatnie miejsce
            }
        }
        private void SortNew()
        {//funkcja sotuje nowo dodany element tak aby zostala zachowana struktura kopca
            int temp;//pomocnicza zmienna do zaminy wartosci
            double j,i;//indeks syna

            i = size-1;//indeks ostatniego elementu
            j = ((i / 2.0) - 1);//indeks rodzica ostatniego elementu
            j=Math.Ceiling(j);//zaokraglanie w gore indeksu syna

            while (j>=0)
           {
                if (mount[Convert.ToInt32(i)] > mount[Convert.ToInt32(j)])//jezeli syn > niz rodzic to zamieniamy wartosciami
                {
                    temp = mount[Convert.ToInt32(i)];//przypisujemy wartosc syna
                    mount[Convert.ToInt32(i)] = mount[Convert.ToInt32(j)];//za wartosc syna wstawiamy wartosc rodzica
                    mount[Convert.ToInt32(j)] = temp;//za wartosc rodzica wstawiamy wartosc syna
                }
                if (j == 0) break;//jezeli dojdziemy do korzenia konczymy
                i = j;//do wartosci syna przypisujemy wartosc jego rodzica
                j = ((Convert.ToDouble(i) / 2.0) - 1);//indeks rodzica ostatniego elementu
                j = Math.Ceiling(j);//zaokraglanie w gore indeksu syna
            }
        }
        private void CloneMound()//kopiowanie aktualneo kopca
        {
            if (size != 0)
            {
                int currentIndex = 0;//zaczynamy od 0
                tempMound = new int[mount.Length];//torzymy pomocniczy kopiec o rozmiarach oryginalnego kopca
                foreach (int data in mount)
                {
                    tempMound[currentIndex] = data;//kopiujemy wszystkie dane z oryginalngo kopca do temp.
                    currentIndex++;//zwiekszamy indeks
                }
                currentIndex = 0;//zerujemy indeks
            }
            else return;
        }
        public void PushForReadFromFile(int add)//funkcja wczytujaca dane z pliku do kopca zachowujac jego strukture
        {            int i, j;

            i = w++;
            j = (i - 1) / 2;

            while (i > 0 && mount[j] < add)
            {
                mount[i] = mount[j];
                i = j;
                j = (i - 1) / 2;
            }
            mount[i] = add;
        }
        public void DeleteFirst()
        {//usuwanie ze szczytu kopca           
            if (size == 0)//jezeli kopiec jest pusty nic nie robimy
                return;
           if(size==1)
            {
                mount = new int[0]; return;//jezeli w kopcu jest 1 element tworzymy pusty kopiec
            }
            CloneMound();//kopiowanie kopca
            mount = new int[size - 1];//tworzymy nowy kopiec o rozmiarze o 1 mniejszym
            mount[0] = tempMound[size-1];//na pierwsze miejsce wpisujemy ostatni element

            for(int i=1;i<size-1;i++)//dla elementow kopca od 1 do ostatniego
            {
                mount[i] = tempMound[i];//kopiujemy zawartosc z tymczasowego kopca
            }
            SortAfterDelete();//sortujemy kopiec aby zachowac jego wlasnosci
            size--;//zmiejszamy rozmiar
        }
        private void SortAfterDelete()//przywracanie struktury kopca po usunieciu elementu ze szczytu kopca
        {
            int temp;//zmienna pomocnicza
            double j, i;//indeks rozica i syna

            i = 0;//indeks rodzica
            j = 2 * i + 1;//indeks lewego syna
            while (j< size-1)
            {
                if(j+2==size)
                {
                    if (mount[Convert.ToInt32(i)] < mount[Convert.ToInt32(j)])//jezeli rodzic mniejszy od syna
                    {
                        temp = mount[Convert.ToInt32(i)];//do zmiennaj pomocniczej przypisujemy wartosc rodzica
                        mount[Convert.ToInt32(i)] = mount[Convert.ToInt32(j)];//do wartosci rodzica wartosc syna
                        mount[Convert.ToInt32(j)] = temp;//do wartosci syna wartosc rodzica
                    }
                    break;
                }
                if (mount[Convert.ToInt32(i)] < mount[Convert.ToInt32(j)] && mount[Convert.ToInt32(j)] > mount[Convert.ToInt32(j + 1.0)])//jezeli rodzic mniejszy od syna i lewy syn > od prawego syna
                {//kopiowanie jak wyzej
                    temp = mount[Convert.ToInt32(i)];
                    mount[Convert.ToInt32(i)] = mount[Convert.ToInt32(j)];
                    mount[Convert.ToInt32(j)] = temp;
                }
                else if (mount[Convert.ToInt32(i)] < mount[Convert.ToInt32(j + 1.0)] && mount[Convert.ToInt32(j)] < mount[Convert.ToInt32(j + 1.0)])//jezeli rodzic mniejszy od syna i lewy syn < od prawego syna
                {
                    j++;//zwiekszamy indeks syna i kopiujemy
                    temp = mount[Convert.ToInt32(i)];
                    mount[Convert.ToInt32(i)] = mount[Convert.ToInt32(j)];
                    mount[Convert.ToInt32(j)] = temp;
                }
                i = j;//za indeks rodzica podstawiamy indeks syna
                j = 2 * i + 1;//obliczamy nowyindeks lewego syna
            }
        }
        public override string ToString()
        {
            if (mount == null) return "";//tablica pusta
            else
            {
                string temp = "";
                for (int i = 0; i < mount.Length; i++)//petla przechodzi przez wszystkie elementy kopca
                {
                    temp += mount[i].ToString()+ Environment.NewLine;//zamiana kolejnych elementow na string i sklejanie w 1 stringa
                }
                return temp;
            }
        }
        public void ReadFromFile(string path)
        {//wczytywanie z pliku

           // Clear();
            StreamReader sr = new StreamReader(path);
            string line;
            int i = 0;
            while ((line = sr.ReadLine()) != null)//zczytywanie z pliku po jednej linii az do konca
            {
                if (i == 0) { size = Convert.ToInt32(line); mount = new int[size]; }//pierwsza linia to rozmiar kopca
                else
                {
                    PushForReadFromFile(Convert.ToInt32(line));//wpianie do kopca liczby z i-tej linii
                }
                i++;
            }
            i = 0;
            w = 0;
        }  
        public void Clear()
        {//czyszzenie kopca
            while(size>0)
            {
                DeleteFirst();
            }
        }
        public Boolean Search(int value)
        {//wyszukianie zadanego elementu
            for (int i = 0; i < size; i++)
            {
                if (mount[i] == value)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
