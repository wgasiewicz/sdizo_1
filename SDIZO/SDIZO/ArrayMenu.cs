﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace SDIZO
{
    public partial class ArrayMenu : Form
    {
        Array arr=new Array();
        Functions call = new Functions();
        public ArrayMenu()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {//usun pierwszy element 
                Stopwatch stopwatch = Stopwatch.StartNew();
                arr.deleteFirst();
                stopwatch.Stop();
                textBox1.Text = stopwatch.Elapsed.ToString();
                richTextBox1.Text = arr.ToString();             
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {//czas operacji

        }

        private void button2_Click(object sender, EventArgs e)
        {//dodaj element na pierwsze miejsce            
            if(call.isNumeric(textBox2.Text)==true)
            {
                Stopwatch stopwatch = Stopwatch.StartNew();
                arr.push(Convert.ToInt32(textBox2.Text));
                stopwatch.Stop();
                textBox1.Text = stopwatch.Elapsed.ToString();
                    richTextBox1.Text = arr.ToString();
            }
            else
            {
                MessageBox.Show("Podaj poprawną liczbę!", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBox2.Clear();
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {//element do dodania

        }

        private void button3_Click(object sender, EventArgs e)
        {//generowanie losowo do tablicy

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {//ilosc elementow

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {//miejsce do dodania

        }

        private void button4_Click(object sender, EventArgs e)
        {//dodaj element w dowolne miejsce
            if (call.isNumeric(textBox2.Text)==true &&call.isNumeric(textBox4.Text)==true)//wybrane miejsce i wartosc musza byc liczbami
            {
                if (arr.testIndex(Convert.ToInt32(textBox4.Text)) == false) { MessageBox.Show("IdexOutOfBounds", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                else
                {
                    Stopwatch stopwatch = Stopwatch.StartNew();
                    arr.pushAny(Convert.ToInt32(textBox2.Text), Convert.ToInt32(textBox4.Text));
                    stopwatch.Stop();
                    textBox1.Text = stopwatch.Elapsed.ToString();
                    richTextBox1.Text = arr.ToString();
                }
            }
            else
            {
                MessageBox.Show("Podaj poprawną liczbę!", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBox2.Clear(); textBox4.Clear();
            }
        }

        private void ArrayMenu_Load(object sender, EventArgs e)
        {

        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {//pole do wyswietlania tablicy

        }

        private void button6_Click(object sender, EventArgs e)
        {//usun element o zadanym indexie
            if (call.isNumeric(textBox5.Text) == true && arr.testIndex(Convert.ToInt32(textBox5.Text)))
            {
                if (arr.testIndex(Convert.ToInt32(textBox5.Text)) == false) { MessageBox.Show("IdexOutOfBounds", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                else
                {
                    Stopwatch stopwatch = Stopwatch.StartNew();
                    arr.deleteIndex(Convert.ToInt32(textBox5.Text));
                    stopwatch.Stop();
                    textBox1.Text = stopwatch.Elapsed.ToString();
                    richTextBox1.Text = arr.ToString();
                }
            }
            else
            {
                MessageBox.Show("Podaj poprawną liczbę!", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBox5.Clear();
            }
        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {//index do usuniecia

        }

        private void button7_Click(object sender, EventArgs e)
        {//usun ostatni element
                Stopwatch stopwatch = Stopwatch.StartNew();
                arr.deleteLast();
                stopwatch.Stop();
                textBox1.Text = stopwatch.Elapsed.ToString();
                richTextBox1.Text = arr.ToString();
        }       

        private void button8_Click(object sender, EventArgs e)
        {// wyswietl tablice
                richTextBox1.Text = arr.ToString();
        }

        private void button5_Click(object sender, EventArgs e)
        {//dodanie elementu na koniec tablicy
            if (call.isNumeric(textBox2.Text)==true)
            {
                Stopwatch stopwatch = Stopwatch.StartNew();
                arr.pushBack(Convert.ToInt32(textBox2.Text));
                stopwatch.Stop();
                textBox1.Text = stopwatch.Elapsed.ToString();
                richTextBox1.Text = arr.ToString();
            }
            else
            {
                MessageBox.Show("Podaj poprawną liczbę!", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBox2.Clear();
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            arr.readFromFile(call.chooseFile());//odczytanie wartosci z wybranego pliku
            richTextBox1.Text = arr.ToString();//wyswietlenie
        }

        private void button10_Click(object sender, EventArgs e)
        {//czyszczenie
            richTextBox1.Clear();
        }

        private void textBox3_TextChanged_1(object sender, EventArgs e)
        {//element do wyszukania

        }

        private void button3_Click_1(object sender, EventArgs e)
        {//wyszukaj element o danej wartosci
            if (call.isNumeric(textBox3.Text) == true)
            {
                Stopwatch stopwatch = Stopwatch.StartNew();
                arr.Search(Convert.ToInt32(textBox3.Text));
                stopwatch.Stop();
                textBox1.Text = stopwatch.Elapsed.ToString();
                richTextBox1.Text = arr.ToString();
                if (arr.Search(Convert.ToInt32(textBox3.Text)) == true)
                {
                    MessageBox.Show("Podany element istnieje w tablicy!", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Podany element nieistnieje w tablicy!", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                textBox3.Clear();
            }
            else
            {
                MessageBox.Show("Podaj poprawną liczbę!", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBox3.Clear();
            }
        }
    }
}
