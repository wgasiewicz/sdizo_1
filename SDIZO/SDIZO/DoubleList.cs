﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SDIZO
{
    class DoubleList
    {//usuwanie i odawanie w dowolne msc,usuwanie ostatniego - do zrobienia
        Node head,tail,current;
        int size;//licznik rozmiaru listy
      public DoubleList()//konstruktor listy
        {
            head = null;
            current = head;
        }
        public void PushFirst(DoubleList list,int add)
        {//dodawanie na pierwsze miejsce listy            
            Node newNode = new Node(add);//tworzymy nowy wierzcholek
            if (head != null)//jezeli pierwszy element istnieje
            {
                head.prev = newNode;//za element przed aktualnym pierwszym elementem podstawiamy nowy wierzcholek
                newNode.next = head;//po nowym wierzcholku ustaiamy wczesniejszy pierwszy element
                head = newNode;
                tail = GetLastNode(list);//za ostatni element przypisujemy ostatni element z aktualnej listy
            }
            else//jezeli lista jest pusta
            {
                head = newNode;//wartosc pierwszego elementu= nowy element
                tail = newNode;//wartosc ostatniego elementu = nowy element
            }           
                        
        size++;//zwiekszamy rozmiar
        }
        public void PushAtLast(DoubleList doubleList,int add)
        {//dodawanie na koniec listy
            Node newNode = new Node(add);//tworzymy nowy wierzcholek
            if (doubleList.head == null)//jezeli pierwszy element listy nie istnieje(lista pusta)
            {
                newNode.prev = null;//element przed nowym nie istneje
                doubleList.head = newNode;//pierwszy element przyjmuje wartosc nowego elementu
                size++;//zwiekszamy rozmiar
                return;
            }
            else//jezeli lista nie jest pusta
            {
                Node lastNode = GetLastNode(doubleList);//wyszukujemy ostatni element aktualnej listy
                lastNode.next = newNode;//element przed ostatnim elementem ustawiamy nowy lemnt
                newNode.prev = lastNode;//element za nowym elementem ustawiamy ostatni elelment
               doubleList.tail = lastNode;//wpisujemy do tail ostatni element
            }
            size++;
        }
        public void PushAtAny(int add,int index,DoubleList list)
        {//dodawani na dowolna pozycje
            size++;//zwiekszenie rozmiaru           

            if (index > size || index < 0) return;//jezeli index nieprawidlowy konczymy
            if (index == size) PushAtLast(list,add);//jezeli pozycja == rozmiar dodajemy na koniec
            if (index == 0) PushFirst(list, add);// jezeli pozycja ==0 dodajemy na poczatek
            else
            {
                Node newNode = new Node(add);//tworzymy nowy wierzcholek
                int currentIndex = 0;//pomocniczy index
                Node actual = head;//aktualny element
                Node prev = null;//element poprzedni

                while (currentIndex < index)//jezeli aktualny index < od zadanego
                {
                    prev = actual;
                    actual = actual.next;
                    currentIndex++;
                }
                if (index == 0)
                {
                    newNode.prev = head.prev;
                    newNode.next = head;
                    head.prev = newNode;
                    head = newNode;
                }
                else if (index == size - 1)
                {
                    Node last = GetLastNode(list);
                    newNode.prev = last;
                    last.next = newNode;
                    newNode = last;
                }
                else
                {
                    newNode.next = prev.next;
                    prev.next = newNode;
                    newNode.prev = actual.prev;
                    actual.prev = newNode;
                }
            }
            
        }
        public void DeleteFirst()
        {//usuwanie pierwszego elementu
            if (size == 0) return;//jezeli lista pusta konczymy
            size--;//zmiejszamy rozmiar            
            if (size == 1)//jezeli rozmiar ==1
            {
                head = null;//zerujemy glowe
                tail = null;//zarujemy ogon
            }
            else head= head.next;//inaczej, przesuwamy glowe na wartosc kolejnego elementu
            
        }
        public void DeleteLast(DoubleList list)
        {//usuwanie ustatniego elementu
            Node last = GetLastNode(list);//wyszukujemy ostatni elelemnt w liscie
            size--;//zmniejszamy rozmiar
            if (size < 1) return;//jezeli rozmiar <1 konczymy
            if (size == 1)//jezeli rozmiar ==1
            {
                head = null;//zerujemy glowe
                tail = null;//zerwujemy ogon
            }
            else
            {
                last.prev.next = null;//usuwamy ostatni element
                tail = last.prev;//za ogon podsawiamy element przed ostatnim
            }
        }
        public void DeleteAny(int index)
        {//usuwanie dowolnego elementu
            Node temp = this.head;//do pomocniczego wierzcholka przypisujemy aktualna glowe
            Node prev = null;//
            int count = 0;//licznik

            size--;//zmniejszamy rozmiar
            while (count < index)
            {
                prev = temp;
                temp = temp.next;
                count++;
            }

            if (count == 0) { this.head = null; }
            else
            {
                if (prev == null)
                {
                    this.head = temp.next;
                    this.head.prev = null;
                }

                if (index == count - 1)
                {
                    prev.next = temp.next;
                    tail = prev;
                    temp = null;
                }
                else if(size==1)//jezeli rozmiar ==1
                {
                    head = null;//zerujemy glowe
                    tail = null;//zarujemy ogon
                }
                else
                {
                    prev.next = temp.next;
                    temp.next.prev = prev;
                }
            }
               
        }
        public override string ToString()
        {//metoda wyswietlajaca liste
            string temp = "";//pomocniczy string
            Node actual = head;//pomocniczy wierzcholek, przypisujemy mu aktualna glowe
            if (size-1 == 0) return temp;//jezeli lista pusta konczymy
            else
            {
                if (actual.next == null)//jezeli nie istnieje kolejny element
                {
                    temp += actual.value + Environment.NewLine;//do stringa dodajemy aktualna wartosc
                    return temp;//zwracamy string
                }
                while (actual != null)//jezeli aktualny wierzcholek istnieje
                {
                    temp += actual.value + Environment.NewLine;//zapisujemy do stringa jego wartosc
                    actual = actual.next;//przechodzimy na nastepny wierzcholek
                }
                return temp;//zwracamy stringa
            }
        }
        private void PushForReadFromFile(DoubleList doubleList,int add)
        {//funkcja do wczytania danych z pliku do listy
            Node newNode = new Node(add);
            if (doubleList.head == null)
            {
                newNode.prev = null;
                doubleList.head = newNode;
                size++;
                return;
            }
            else
            {
                Node lastNode = GetLastNode(doubleList);
                lastNode.next = newNode;
                newNode.prev = lastNode;
            }
        }
        public void ReadFromFile(DoubleList list,string path)
        {//odczyt z pliku
            Clear();//czyscimy tablice
            StreamReader sr = new StreamReader(path);
            string line;//string do odczytu linii
            int i = 0;//licznik linii
            while ((line = sr.ReadLine()) != null)
            {
                if (i == 0) { size = Convert.ToInt32(line); }
                else
                {
                    PushForReadFromFile(list,Convert.ToInt32(line));
                }
                i++;
            }
            i = 0; 
        }
        Node GetLastNode(DoubleList list)
        {//funkcja zwracajaca ostani element listy
            Node temp = list.head;
            while (temp.next != null)//dopoki istnieje ostatni element
            {
                temp = temp.next;//zapisujemy go
            }
            return temp;
        }
        void Clear()//czyszczenie listy
        {
            int count = 0;//zmienna pomocnicza
            while (head!=null)//dopuki istnieje glowa
                DeleteAny(count);            //usuwamy pierwszy element
        }
        public Boolean TestIndex(int index)//sprawdzenie czy zadany index nie przekracza rozmiaru
        {
            if (index < 0 || index > size)//jezeli index mniejszy o 0 albo wiekszy od rozmiaru
                return false;//zwracamy falsz
            else return true;//inaczej zwracamy true
        }
        public Boolean Search(int value)
        {
            Node actual = head;//aktualny element
            Node prev = null;//element poprzedni
            int currentIndex = 0;//pomocniczy inex
            while (currentIndex < size)//przechodzimy cala liste szukajac zadanej wartosci
            {
                if (actual.value == value) return true;//jezeli spotkamy zadana wartosc zwracamy true
                prev = actual;//za element poprzedni przypisujemy element aktualny
                actual = actual.next;//przechodzimi na nastepny element
                currentIndex++;//zwiekszamy index
            }
            return false;//nie udalo sie zwracamy false
        }
    }
}
